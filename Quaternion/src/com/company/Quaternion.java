package com.company;

import java.util.* ;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

    private double r;
    private double i;
    private double j;
    private double k;

    /** Constructor from four double values.
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion (double a, double b, double c, double d) {
        this.r = a;
        this.i = b;
        this.j = c;
        this.k = d;
    }

    /** Real part of the quaternion.
     * @return real part
     */
    public double getRpart() {
        return this.r;
    }

    /** Imaginary part i of the quaternion.
     * @return imaginary part i
     */
    public double getIpart() {
        return this.i;
    }

    /** Imaginary part j of the quaternion.
     * @return imaginary part j
     */
    public double getJpart() {
        return this.j;
    }

    /** Imaginary part k of the quaternion.
     * @return imaginary part k
     */
    public double getKpart() {
        return this.k;
    }

    /** Conversion of the quaternion to the string.
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String imrk = "+";
        String jmrk = "+";
        String kmrk = "+";

        if (this.i < 0) {
            imrk = "";
        }
        if (this.j < 0) {
            jmrk = "";
        }
        if (this.k < 0) {
            kmrk = "";
        }

        return this.r + imrk + this.i + "i" + jmrk + this.j + "j" + kmrk + this.k + "k";
    }

    /** Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     * @throws IllegalArgumentException if string s does not represent
     *     a quaternion (defined by the <code>toString</code> method)
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * http://www.vogella.com/tutorials/JavaRegularExpressions/article.html
     */

    public static Quaternion valueOf (String s) {

        if((s.trim().charAt(s.trim().length() - 1) == 'k')) {
            char someChar = 'k';
            int count = 0;

            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == someChar) {
                    count++;
                }
            }

            if (count > 1) {
                throw new IllegalArgumentException("JWOAJA");
            }

            Pattern p = Pattern.compile("-?\\d+(?:\\.\\d+)?");
            List<String> num = new ArrayList<>();
            Matcher m = p.matcher(s);
            while (m.find()) {
                num.add(m.group());
            }
            if (num.size() == 4) {

                double r = Double.valueOf(String.valueOf(num.get(0)));
                double i = Double.valueOf(String.valueOf(num.get(1)));
                double j = Double.valueOf(String.valueOf(num.get(2)));
                double k = Double.valueOf(String.valueOf(num.get(3)));

                return new Quaternion(r, i, j, k);
            } else {
                throw new IllegalArgumentException("This string '" + s + "' is not a quaternion!");
            }
        } else {
            throw new IllegalArgumentException("This string '" + s + "' is not a quaternion!");
        }

    }

    /** Clone of the quaternion.
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(r, i, j, k);
    }

    /** Test whether the quaternion is zero.
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return equals(new Quaternion(0., 0., 0., 0.));
    }

    /** Conjugate of the quaternion. Expressed by the formula
     *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(this.r, -this.i, -this.j, -this.k);
    }

    /** Opposite of the quaternion. Expressed by the formula
     *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-this.r, -this.i, -this.j, -this.k);
    }

    /** Sum of quaternions. Expressed by the formula
     *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus (Quaternion q) {
        return new Quaternion(this.r + q.r, this.i + q.i, this.j + q.j, this.k + q.k);
    }

    /** Product of quaternions. Expressed by the formula
     *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times (Quaternion q) {
        return new Quaternion(
                this.r * q.r - this.i * q.i - this.j * q.j - this.k * q.k,
                this.r * q.i + q.r * this.i + this.j * q.k - this.k * q.j,
                this.r * q.j - this.i * q.k + this.j * q.r + this.k * q.i,
                this.r * q.k + i * q.j - this.j * q.i + this.k * q.r);
    }

    /** Multiplication by a coefficient.
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times (double r) {
        return new Quaternion(this.r * r, this.i * r, this.j * r, this.k * r);
    }

    /** Inverse of the quaternion. Expressed by the formula
     *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (isZero())
            throw new RuntimeException("Cannot divide with zero.");
        double rriijjkk = this.r * this.r + this.i * this.i + this.j * this.j + this.k * this.k;
        return new Quaternion(
                this.r / rriijjkk,
                (-this.i) / rriijjkk,
                (-this.j) / rriijjkk,
                (-this.k) / rriijjkk);
    }

    /** Difference of quaternions. Expressed as addition to the opposite.
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus (Quaternion q) {
        return this.plus(q.opposite());
    }

    /** Right quotient of quaternions. Expressed as multiplication to the inverse.
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight (Quaternion q) {
        if (q.isZero()) throw new RuntimeException("Cannot divide with zero");
        return this.times(q.inverse());
    }

    /** Left quotient of quaternions.
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft (Quaternion q) {
        if (q.isZero()) throw new RuntimeException("Cannot divide with zero");
        return q.inverse().times(this);
    }

    /** Equality test of quaternions. Difference of equal numbers
     *     is (close to) zero.
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     * https://gamedev.stackexchange.com/questions/75072/how-can-i-compare-two-quaternions-for-logical-equality
     */
    @Override
    public boolean equals (Object qo) {
        double closeToZero = 0.0001;
        if (qo instanceof Quaternion)
            return (
                    Math.abs(this.r - ((Quaternion) qo).r) < closeToZero &&
                            Math.abs(this.i - ((Quaternion) qo).i) < closeToZero &&
                            Math.abs(this.j - ((Quaternion) qo).j) < closeToZero &&
                            Math.abs(this.k - ((Quaternion) qo).k) < closeToZero
            ) ? true : false;
        else
            return false;
    }

    /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     * @param q factor
     * @return dot product of this and q
     * https://android.googlesource.com/platform/external/jmonkeyengine/+/master/engine/src/core/com/jme3/math/Quaternion.java
     */
    public Quaternion dotMult (Quaternion q) {
        Quaternion x = times(q.conjugate()).plus(q.times(conjugate()));
        return new Quaternion(x.r / 2., x.i / 2., x.j / 2., x.k / 2.);
    }

    /** Integer hashCode has to be the same for equal objects.
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /** Norm of the quaternion. Expressed by the formula
     *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(this.r * this.r + this.i * this.i + this.j * this.j + this.k * this.k);
    }

    /** Main method for testing purposes.
     * @param arg command line parameters
     */
    public static void main (String[] arg) {
        Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf (arg[0]);
        System.out.println ("first: " + arv1.toString());
        System.out.println ("real: " + arv1.getRpart());
        System.out.println ("imagi: " + arv1.getIpart());
        System.out.println ("imagj: " + arv1.getJpart());
        System.out.println ("imagk: " + arv1.getKpart());
        System.out.println ("isZero: " + arv1.isZero());
        System.out.println ("conjugate: " + arv1.conjugate());
        System.out.println ("opposite: " + arv1.opposite());
        System.out.println ("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion)arv1.clone();
        } catch (CloneNotSupportedException e) {};
        System.out.println ("clone equals to original: " + res.equals (arv1));
        System.out.println ("clone is not the same object: " + (res!=arv1));
        System.out.println ("hashCode: " + res.hashCode());
        res = valueOf (arv1.toString());
        System.out.println ("string conversion equals to original: "
                + res.equals (arv1));
        Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf (arg[1]);
        System.out.println ("second: " + arv2.toString());
        System.out.println ("hashCode: " + arv2.hashCode());
        System.out.println ("equals: " + arv1.equals (arv2));
        res = arv1.plus (arv2);
        System.out.println ("plus: " + res);
        System.out.println ("times: " + arv1.times (arv2));
        System.out.println ("minus: " + arv1.minus (arv2));
        double mm = arv1.norm();
        System.out.println ("norm: " + mm);
        System.out.println ("inverse: " + arv1.inverse());
        System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
        System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
        System.out.println ("dotMult: " + arv1.dotMult (arv2));
        System.out.println(valueOf("-1-2j-3i-4k"));
    }
}
// end of file
