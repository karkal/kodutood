package com.company;

import java.util.Iterator;
import java.util.LinkedList;

//https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html
public class LongStack {

    public static void main (String[] argum) {
        LongStack st2kk = new LongStack();
        LongStack teineSt2kk = new LongStack();
        st2kk.push(1321315L);
        st2kk.push(11213131L);
        st2kk.op("*");
        teineSt2kk.push(1321315L);
        teineSt2kk.push(11213131L);
        System.out.println(st2kk.toString());
        System.out.println(st2kk.equals(teineSt2kk));
        System.out.println(st2kk.stEmpty());
        System.out.println(teineSt2kk.toString());
        System.out.println(interpret("-234 9"));
    }

    private LinkedList<Long> mag = new LinkedList<>();

    LongStack() {
    }

    //https://www.tutorialspoint.com/java/util/linkedlist_clone.htm
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            LongStack kloon = new LongStack();
            kloon.mag.addAll(mag);
            return kloon;
        } catch (Exception e) {
            throw new CloneNotSupportedException("Ei õnnestunud kloonida");
        }
    }

    public boolean stEmpty() {
        return mag.isEmpty();
    }

    public void push (long a) {
        mag.push(a);
    }

    public long pop() {
        if (stEmpty())
            throw new RuntimeException("Ei saa stackist eemaldada, stack on tühi." + mag);
        return mag.pop();
    }

    public void op (String s) {

        if (s != "+" && s != "-" && s != "/" && s != "*") throw new RuntimeException("Teie antud tehtemärk: '" + s +
                "' ei sobi avaldisse.");
        if (mag.size() >= 2) {
            long el2 = pop();
            long el1 = pop();
            if (s == "+") push(el1 + el2);
            if (s == "-") push(el1 - el2);
            if (s == "*") push(el1 * el2);
            if (s == "/") push(el1 / el2);
        } else {
            throw new RuntimeException("Ei saa arvutada, stack on tühi." + mag);
        }

    }

    public long tos() {
        if (stEmpty())
            throw new RuntimeException("Ei saa stacki piiluda, stack on tühi." + mag);
        return mag.peek();
    }

    @Override
    public boolean equals (Object o) {
        if (o instanceof LongStack){
            return mag.equals(((LongStack)o).mag);
        } else {
            throw new ClassCastException("Sisend pole longstack tüüpi objekt.");
        }
    }

    //https://stackoverflow.com/questions/24335906/linkedlist-to-a-string
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        Iterator<Long> it = mag.descendingIterator();
        while (it.hasNext()) {
            sb.append("[" + it.next() + "] ");
        }
        /* for (int i = mag.size() - 1; i >= 0; i--) {
            sb.append(String.valueOf("[" + mag.get(i) + "] "));
        } */
        return sb.toString();
    }


    // https://stackoverflow.com/questions/2563608/check-whether-a-string-is-parsable-into-long-without-try-catch
    public static boolean isNumeric(String s) {
        try {
            Long.parseLong(s);
        } catch (RuntimeException ex) {
            return false;
        }
        return true;
    }


    //https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
    //https://stackoverflow.com/questions/7899525/how-to-split-a-string-by-space
    //https://www.tutorialspoint.com/java/util/stringtokenizer_counttokens.htm
    public static long interpret(String pol) {


        if (pol == null || pol.length() == 0)
            throw new RuntimeException("Avaldis ei saa olla tühi.");



        long number = 0;

        //https://www.geeksforgeeks.org/split-string-java-examples/
        //https://stackoverflow.com/questions/15625629/regex-expressions-in-java-s-vs-s
        String[] splited = pol.trim().split("\\s+");

        /*StringTokenizer tokens = new StringTokenizer(pol, " ");
          String[] splited = new String[tokens.countTokens()];
          int y = 0;
            while(tokens.hasMoreTokens()){
            splited[y] = tokens.nextToken();
            ++y;
        } */


        String operators = "+-*/";
        LongStack stack = new LongStack();

        for(String t : splited) {


            if (!t.equals("+") && !t.equals("-") && !t.equals("*") && !t.equals("/") && isNumeric(t) == false)
                throw new RuntimeException("Ei saa tehet lahendada, vale sümbol: '" + t + "' tehtes:" + pol);

            if(!operators.contains(t)) {
                stack.push(Long.parseLong(t));

            } else if (stack.mag.size() < 2 && t != " " && t != "\t") {
                throw new RuntimeException("Liiga vähe arve avaldises: (" + pol + ")");

            } else {
                long a = Long.valueOf(stack.pop());
                long b = Long.valueOf(stack.pop());
                int index = operators.indexOf(t);
                switch (index) {
                    case 0:
                        stack.push(Long.valueOf(a + b));
                        break;
                    case 1:
                        stack.push(Long.valueOf(b - a));
                        break;
                    case 2:
                        stack.push(Long.valueOf(a * b));
                        break;
                    case 3:
                        stack.push(Long.valueOf(b / a));
                        break;
                }
            }
        }

        if (stack.mag.size() != 1) throw new RuntimeException("Liiga palju arve avaldses: '" + pol + "' peale tehet peab olema magasinis 1 element, teil on: " +
                stack.mag.size());

        number = Long.valueOf(stack.pop());
        return number;
    }

}


