package com.company;

/*
* https://docs.oracle.com/javase/7/docs/api/java/lang/Enum.html
* http://math.hws.edu/eck/cs124/javanotes8/c7/s4.html
* https://stackoverflow.com/questions/4441099/how-do-you-count-the-elements-of-an-array-in-java
* */

public class Main {

    enum Animal {sheep, goat};

    public static void main (String[] param) {
        Animal[] loom = new Animal[10];
        loom[0]=Animal.sheep;
        loom[1]=Animal.goat;
        loom[2]=Animal.goat;
        loom[3]=Animal.sheep;
        loom[4]=Animal.goat;
        loom[5]=Animal.sheep;
        loom[6]=Animal.goat;
        loom[7]=Animal.goat;
        loom[8]=Animal.sheep;
        loom[9]=Animal.goat;

        reorder(loom);


        for (Animal animal : loom) {
            System.out.println(animal);
        }

    }

    public static void reorder (Animal[] animals) {

        int lambaAsukoht = 0;
        int kitseAsukoht;

        //Esimene for tsükkel loeb array'st üle iga kitse ja lükkab esimese lamba asukoha nii palju edasi, kui kaugel viimane kits on

        for (int i = 0; i < animals.length; i++) {
            if (animals[i] == Animal.goat) {
                lambaAsukoht++;
            }
        }


        //Teine for tsükkel annab igale kitsele nullist alates indexi numbri kuni see on < lambaAsukoht (see eelmisest tsüklist)

        for(kitseAsukoht = 0; kitseAsukoht < lambaAsukoht; kitseAsukoht++) {
            animals[kitseAsukoht] = Animal.goat;
            //System.out.println(kitseAsukoht + " kits ");
        }


        //Kolmas for tsükkel annab igale lambale alates viimase kitse indexist kuni array lõpuni indexi

        for (lambaAsukoht = kitseAsukoht; lambaAsukoht < animals.length; lambaAsukoht++) {
            animals[lambaAsukoht] = Animal.sheep;
            //System.out.println(lambaAsukoht + " lammas ");
        }
    }
}
