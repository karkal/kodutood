package com.company;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {
    private int[][] connected1;
    private int[][] connected2;
    private boolean graph1 = false;

    /** Main method.
     *
     * */
    public static void main (String[] args) {
        GraphTask a = new GraphTask();
        a.run("Test2_1", 5, 4, "Test2_2", 5, 6, true);

    }

    /** Actual main method to run examples and everything.
     * @param Graph1 name of the first graph.
     * @param Graph2 name of the second graph.
     * @param g1N amount of vertices in Graph1.
     * @param g1M amount of arcs in Graph1.
     * @param g2N amount of vertices in Graph2.
     * @param g2M amount of arcs in Graph2.
     * @param print boolean for printing the adjacency matrices of Graph1, Graph2 and SumGraph.
     * */
    public void run(String Graph1, int g1N, int g1M, String Graph2, int g2N, int g2M, boolean print) {
        Graph g = new Graph (Graph1);
        g.createRandomSimpleGraph (g1N, g1M, print);
        System.out.println (g);
        graph1 = true;
        Graph o = new Graph (Graph2);
        o.createRandomSimpleGraph (g2N, g2M, print);
        System.out.println(o);
        Graph s = new Graph("Sum of: " + Graph1 + " and " + Graph2);
        s.addition(s);
        System.out.println(s);
        System.out.println("-------------------------------------------------------------------------------------------");
        graph1 = false;
    }

    // Create a method for adding two graphs.
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex (String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    /** Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;

        Arc (String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc (String s) {
            this (s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        private int [][] connected;

        Graph (String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph (String s) {
            this (s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty ("line.separator");    //    G (info)
            StringBuffer sb = new StringBuffer (nl);              //    A --> AB (A->B) AD (A->D)
            sb.append (id);                                       //    B -->
            sb.append (nl);                                       //    C -->
            Vertex v = first;                                     //    D --> DB (D->B) DC (D->C)
            while (v != null) {
                sb.append (v.toString());
                sb.append (" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append (" ");
                    sb.append (a.toString());
                    sb.append (" (");
                    sb.append (v.toString());
                    sb.append ("->");
                    sb.append (a.target.toString());
                    sb.append (")");
                    a = a.next;
                }
                sb.append (nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex (String vid) {
            Vertex res = new Vertex (vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc (String aid, Vertex from, Vertex to) {
            Arc res = new Arc (aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         * @param n number of vertices added to this graph
         */
        public void createRandomTree (int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex [n];
            for (int i = 0; i < n; i++) {
                varray [i] = createVertex ("v" + String.valueOf(n-i));
                if (i > 0) {
                    int vnr = (int)(Math.random()*i);
                    createArc ("a" + varray [vnr].toString() + "_"
                            + varray [i].toString(), varray [vnr], varray [i]);
                    createArc ("a" + varray [i].toString() + "_"
                            + varray [vnr].toString(), varray [i], varray [vnr]);
                } else {}
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int [info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res [i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph (int n, int m, boolean print) {
            if (n < 0)
                throw new IllegalArgumentException ("The amount of vertices can't be negative: " + n + "!");
            if (n == 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException ("Too many vertices: " + n);
            if (m < n-1 || m > n*(n-1)/2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree (n);       // n-1 edges created here
            Vertex[] vert = new Vertex [n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int)(Math.random()*n);  // random source
                int j = (int)(Math.random()*n);  // random target
                if (i==j)
                    continue;  // no loops
                if (connected [i][j] != 0 || connected [j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert [i];
                Vertex vj = vert [j];
                createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected [i][j] = 1;
                createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected [j][i] = 1;
                edgeCount--;  // a new edge happily created
            }

            //Print the  given matrices if it is requested.
            if(print){
                System.out.println("Adjacency matrix of " + Graph.this.id);
                for (int[] aConnected : connected) {
                    for (int anAConnected : aConnected) {
                        System.out.print(anAConnected + " ");
                    }
                    System.out.println();
                }
            }
        }

        /**
         * Add two graph matrices.
         * Create new graph from the sum of two matrices.
         * Used the createRandomSimpleGraph and createRandomTree method as an example.
         * Used materials:
         * https://examples.javacodegeeks.com/core-java/util/arrays/compare-two-dimensional-arrays/
         * https://en.wikibooks.org/wiki/Java_Programming/Preventing_NullPointerException
         * @param Graph3 the sum of first and second graph.
         * @param print boolean for printing the adjacency matrix.
         */

        public Graph addition(Graph Graph3){
            int[][] connected3;
            Graph3 = new Graph("L");

            int change = 2;

            for (int i = 0; i < connected.length; i++) {
                for (int j = 0; j < connected[i].length; j++) {
                    if (connected[i][j] == 0 && i != j) {
                        connected[i][j] = change;
                    }
                }
            }

            //Declare the array and size for the vertices.
            Vertex[] vert = new Vertex [connected.length];

            //Add vertices to the array starting from the last vertix.
            for (int c = 0; c < connected.length; c++){
                vert [c] = Graph3.createVertex ("v" + String.valueOf(connected.length-c));
            }

            //Loop through all elements of the sum matrix and make the connections between vertices.
            for (int i = 0; i < connected.length; i++) {
                for (int j = 0; j < connected.length; j++) {
                    if(connected[i][j] == 1){
                        Graph3.createArc("a" + vert[connected.length-1-i].toString() + "_"
                                        + vert[connected.length-1-j].toString(), vert[connected.length-1-i],
                                vert[connected.length-1-j]);
                    }
                }
            }

                System.out.println("Adjacency matrix of the sum.");
                for (int[] bConnected : connected) {
                    for (int anBConnected : bConnected) {
                        System.out.print(anBConnected + " ");
                    }
                    System.out.println();
                }

            return Graph3;
        }
    }
}