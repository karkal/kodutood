package com.company;

import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node (String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    public static boolean commaTester(String s) {
        boolean toggle = false;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ',' && s.charAt(i - 2) != ')' && s.charAt(i - 2) != '(' && s.charAt(i + 2) != ')' && s.charAt(i + 2) != ',') {
                toggle = true;
            }
        }
        return toggle;
    }
    // (B)A,(D)C
    // ((E,F,D)B1,C)A
    // (((2,1)-,4)*,(69,3)/)+

    // https://docs.oracle.com/javase/8/docs/api/index.html?java/util/StringTokenizer.html
    // https://stackoverflow.com/questions/2206378/how-to-split-a-string-but-also-keep-the-delimiters#comment2157644_2206386
    // http://interactivepython.org/runestone/static/pythonds/Trees/ParseTree.html
    // https://www.youtube.com/watch?v=PEdFSWJm0TA&t=426s
    public static Node parsePostfix (String s) {
        if (s.contains("\t")) throw new RuntimeException("Don't use tabs: " + s);
        if (commaTester(s)) throw new RuntimeException("Cannot have two roots, add brackets: " + s);
        if (s.endsWith(",") || s.endsWith(")")) throw new RuntimeException("Root node wrong: "  + s);
        if (s == null) throw new RuntimeException("String cannot be empty");
        if (s.contains(",,") || s.contains("),") || s.contains(",)") || s.contains("(,") || s.contains("()"))
            throw new RuntimeException("Node cannot be null: " + s);
        if (s.contains("))")) throw new RuntimeException("Child node must have parent node: " + s);


        StringTokenizer uhu = new StringTokenizer(s, "(),", true);
        ArrayList<String> pieces = new ArrayList<>();


        while (uhu.hasMoreTokens()) {
            pieces.add(uhu.nextToken());
        }

        if (pieces.size() > 3 && pieces.get(pieces.size() - 2).equals(",")) throw new RuntimeException("Cannot have to roots: " + s);

        if (s.contains(",")) {
            if (!s.contains(")") || !s.contains("(")) {
                throw new RuntimeException("Brackets needed: " + s);
            }
        }

        Stack<Node> cont = new Stack<>();
        Node node = new Node(null, null, null);
        boolean toggle2 = false;

        for (String piece : pieces) {
            if (piece.contains(" ")) throw new RuntimeException("Mode cannot contain space: " + piece);
            if (pieces.size() > 4 && !pieces.contains(",")) throw new RuntimeException("Write parent(s) correctly: " + s);

            switch (piece) {
                case "(":
                    cont.push(node);
                    node.firstChild = new Node(null, null, null);
                    node = node.firstChild;
                    break;
                case ",":
                    node.nextSibling = new Node(null, null, null);
                    node = node.nextSibling;
                    if (toggle2) {
                        throw new RuntimeException("Multiple roots, add brackets: "  + s);
                    }
                    break;
                case ")":
                    node = cont.pop();
                    if (cont.size() == 0) {
                        toggle2 = true;
                    }
                    break;
                default:
                    if (node.name == null) {
                        node.name = piece;
                        //System.out.println(node.name);
                    }
                    break;
            }
        }

        if (node.name == null) throw new RuntimeException("Root node cannot be null: " + s);
        return node;
    }

    // https://www.daniweb.com/programming/software-development/threads/476485/expression-tree-parenthesis - postOrder
    // influence
    // https://stackoverflow.com/questions/48624634/java-stringbuilder-append-return
    public String leftParentheticRepresentation() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.name);
        if (this.firstChild != null) {
            builder.append("(");
            builder.append(this.firstChild.leftParentheticRepresentation());
            builder.append(")");
        }
        if (this.nextSibling != null) {
            builder.append(",");
            builder.append(this.nextSibling.leftParentheticRepresentation());
        }
        return builder.toString();
    }

    public static void main (String[] param) {
        String s = "h";
        Node t = Node.parsePostfix (s);
        System.out.println(t.name);
        String v = t.leftParentheticRepresentation();
        System.out.println (s + " ==> " + v); // ((E,F)B1,C)A ==> A(B1(E,F),C)
    }
}